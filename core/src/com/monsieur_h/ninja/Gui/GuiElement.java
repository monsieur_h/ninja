package com.monsieur_h.ninja.Gui;


/**
 * Created by hub on 12/09/15.
 */
public abstract class GuiElement {
    public abstract void draw();

    public abstract void update(float delta);
}
