package com.monsieur_h.ninja.Gui;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Created by hub on 12/09/15.
 */
public class ValidStrokeList extends GuiElement {
    private final float m_width;
    private final float m_strokeWidth;
    private Stroke[] m_strokes;
    private int m_index = 0;
    private Vector2 m_position = new Vector2();
    private ShapeRenderer m_shapeRenderer = new ShapeRenderer();


    public ValidStrokeList(int _size, float _width) {
        m_strokes = new Stroke[_size];
        m_width = _width;

        m_strokeWidth = m_width / _size;
    }

    public void update(float delta) {
        for (int i = m_strokes.length - 1; i >= 0; i--) {
            if (m_strokes[i] == null) {
                continue;
            }
            if (!m_strokes[i].isAlive()) {
                m_strokes[i] = null;
                continue;
            }
            m_strokes[i].act(delta);
        }

        //Removes empty spaces
        for (int i = 0; i < m_strokes.length - 1; i++) {
            if (m_strokes[i] == null && m_strokes[i + 1] != null) {
                m_strokes[i] = m_strokes[i + 1];
                m_strokes[i + 1] = null;
                m_index = i;
            }
        }
    }

    @Override
    public void draw() {
        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (int i = 0; i < m_strokes.length; i++) {
            m_shapeRenderer.rect(
                    i * m_strokeWidth + m_position.x,
                    m_position.y,
                    m_strokeWidth,
                    m_strokeWidth
            );
        }

        for (int i = 0; i < m_index; i++) {
            m_strokes[i].draw();
        }
        m_shapeRenderer.end();
    }

    public void addStroke(Stroke _stroke) {
        if (m_index == m_strokes.length) {
            clear();
        }
        m_strokes[m_index] = _stroke;
        float scale = m_strokeWidth / Math.max(_stroke.getWidth(), _stroke.getHeight());
        _stroke.setScale(scale / 2);
        _stroke.setCenter(new Vector2(m_position.x + (m_index * m_strokeWidth) + m_strokeWidth / 2, m_position.y + m_strokeWidth / 2));
        m_index++;
    }

    private void clear() {
        for (int i = 0; i < m_strokes.length; i++) {
            m_strokes[i] = null;
        }
        m_index = 0;
    }

    public void setPosition(Vector2 _position) {
        m_position.set(_position);
    }

    public float getHeight() {
        return m_strokeWidth;
    }

    public float getWidth() {
        return m_strokeWidth * m_strokes.length;
    }
}
