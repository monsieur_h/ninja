package com.monsieur_h.ninja;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.monsieur_h.ninja.Gui.ValidStrokeList;
import com.monsieur_h.ninja.Stroke.KnownStroke;
import com.monsieur_h.ninja.Stroke.Stroke;
import com.monsieur_h.ninja.Stroke.StrokeManager;
import com.monsieur_h.ninja.Stroke.StrokePoint;
import com.monsieur_h.ninja.Stroke.behavior.ColorAndSizeBehavior;
import com.monsieur_h.ninja.Stroke.behavior.StrokePointIdleBehavior;
import com.monsieur_h.ninja.Stroke.comparators.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Where all the fun happens
 * Created by hub on 07/08/15.
 */
public class MainScene extends Stage implements InputProcessor {
    HashMap<Integer, Stroke> strokePointerHashmap = new HashMap<Integer, Stroke>();
    private StrokeManager strokeManager;
    private ValidStrokeList m_validStrokeList;

    public MainScene() {
        strokeManager = new StrokeManager("strokeDatabase.json");


        strokeManager.getComparatorCollection().add(new LengthComparator());
        strokeManager.getComparatorCollection().add(new AngleComparator(), 1.2f);
        strokeManager.getComparatorCollection().add(new PointsCounterComparator(), .25f);
        strokeManager.getComparatorCollection().add(new AveragePointComparator(), .5f);
        strokeManager.getComparatorCollection().add(new EndPointPositionComparator(), .5f);
        strokeManager.getComparatorCollection().add(new StartPointPositionComparator(), .5f);

        m_validStrokeList = new ValidStrokeList(4, Gdx.graphics.getWidth());
        m_validStrokeList.setPosition(new Vector2(0, Gdx.graphics.getHeight() - m_validStrokeList.getHeight()));

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void draw() {
        super.draw();
        for (Stroke stroke : strokePointerHashmap.values()) {
            stroke.draw();
        }

        m_validStrokeList.draw();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        for (Stroke stroke : strokePointerHashmap.values()) {
            stroke.act(delta);
            if (stroke.getPoints().size() > stroke.MAX_POINTS / 2) {
                strokeManager.getNormalizer().removeByAngle(stroke);
            }
        }
        m_validStrokeList.update(delta);
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!strokePointerHashmap.containsKey(pointer)) {
            strokePointerHashmap.put(pointer, new Stroke());
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        assert (strokePointerHashmap.containsKey(pointer));
        strokePointerHashmap.get(pointer).addPoint(screenToStageCoordinates(new Vector2(screenX, screenY)));
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        assert (strokePointerHashmap.containsKey(pointer));
        KnownStroke k = strokeManager.recognize(strokePointerHashmap.get(pointer));
        if (k != null) {
            Gdx.app.log("RECOGNIZED", "" + k.getName());
            Stroke matchingStroke = k.clone();
            matchingStroke.setBehavior(new StrokePointIdleBehavior());
            matchingStroke.setPointRadius(2f);
//            float duration = StrokePoint.DEFAULT_LIFETIME * 15f;
//            ColorAndSizeBehavior cas = new ColorAndSizeBehavior(
//                    StrokePoint.DEFAULT_MIN_RADIUS,
//                    StrokePoint.DEFAULT_MAX_RADIUS,
//                    Color.WHITE, new Color(1, 1, 1, 0f),
//                    duration);
//            matchingStroke.setBehavior(cas);
            m_validStrokeList.addStroke(matchingStroke);
        } else {
            Gdx.app.log("RECOGNIZED", " Not found");
        }
        strokePointerHashmap.remove(pointer);
        return super.touchUp(screenX, screenY, pointer, button);
    }
}
