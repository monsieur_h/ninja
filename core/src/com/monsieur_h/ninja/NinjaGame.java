package com.monsieur_h.ninja;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class NinjaGame extends ApplicationAdapter {
    private SpriteBatch batch;
    private MainScene scene;

    @Override
    public void create() {
        batch = new SpriteBatch();
        scene = new MainScene();
    }

    @Override
    public void render() {
        scene.act(Gdx.graphics.getDeltaTime());
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        scene.draw();
        batch.end();
    }
}
