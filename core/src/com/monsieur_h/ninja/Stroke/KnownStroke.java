package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.math.Vector2;

/**
 * A stroke from the database
 * Created by hubert on 18/08/15.
 */
public class KnownStroke extends Stroke {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void addPoint(Vector2 position) {
        points.add(new StrokePoint(position));
    }
}
