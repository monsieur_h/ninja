package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Util.Event;

/**
 * Event triggered when a point is added to a line
 * Created by hub on 8/16/15.
 */
public class PointAddedEvent extends Event {

    private Vector2 position;

    public PointAddedEvent(StrokePoint p) {
        position = p.getPosition();
    }

    public Vector2 getPosition() {
        return position;
    }
}
