package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * A segment is an object used to draw a line between two points
 * Created by hubert on 14/08/15.
 */
public class Segment {
    protected StrokePoint p1;
    protected StrokePoint p2;
    protected Color color;
    protected ShapeRenderer shapeRenderer;
    protected Vector2 v1;
    protected Vector2 v2;

    public Segment(StrokePoint p1, StrokePoint p2) {
        shapeRenderer = new ShapeRenderer();
        color = new Color(Color.WHITE);
        v1 = new Vector2();
        v2 = new Vector2();
        init(p1, p2);
    }

    private void init(StrokePoint p1, StrokePoint p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void set(StrokePoint p1, StrokePoint p2) {
        init(p1, p2);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void computeVertices() {
        v1.set(p1.getPosition().cpy().sub(p2.getPosition())); //Difference p1->p2
        v1.nor();
        v1.rotate90(0);
        v1.scl(p1.getRadius() / 2);

        v2.set(p2.getPosition().cpy().sub(p1.getPosition())); //Difference p2->p1
        v2.nor();
        v2.rotate90(-1);
        v2.scl(p2.getRadius() / 2);
    }

    protected void startDraw() {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color);
    }

    protected void endDraw() {
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void draw() {
        computeVertices();
        if (p1 == null || p2 == null)
            return;
        startDraw();

        Color previous = shapeRenderer.getColor();
        shapeRenderer.setColor(p1.getColor());
        shapeRenderer.circle(p1.getPosition().x, p1.getPosition().y, p1.getRadius() / 2, 8);
        shapeRenderer.setColor(previous);

        shapeRenderer.triangle(
                p1.getPosition().x + v1.x,
                p1.getPosition().y + v1.y,
                p1.getPosition().x - v1.x,
                p1.getPosition().y - v1.y,
                p2.getPosition().x - v2.x,
                p2.getPosition().y - v2.y,
                p1.getColor(),
                p1.getColor(),
                p2.getColor()
        );
        shapeRenderer.triangle(
                p1.getPosition().x + v1.x,
                p1.getPosition().y + v1.y,
                p2.getPosition().x - v2.x,
                p2.getPosition().y - v2.y,
                p2.getPosition().x + v2.x,
                p2.getPosition().y + v2.y,
                p1.getColor(),
                p2.getColor(),
                p2.getColor()
        );
        endDraw();
    }
}
