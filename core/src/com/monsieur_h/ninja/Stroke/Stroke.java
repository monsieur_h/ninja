package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Stroke.behavior.StrokePointBehaviorInterface;
import com.monsieur_h.ninja.Stroke.behavior.StrokePointLerpBehavior;
import com.monsieur_h.ninja.Util.Clock;

import java.util.ArrayList;

/**
 * A line drawn by the user
 * The minimum amount of memory reserved for UserPoints is defined by {@link #MAX_POINTS}
 * Created by hub on 07/08/15.
 */
public class Stroke {
    public final int MAX_POINTS = 100;
    private final float DISTANCE_THRESHOLD = 25f;
    private final float TIME_THRESHOLD = .05f;
    protected ArrayList<StrokePoint> points = new ArrayList<StrokePoint>();
    private Clock clock = new Clock();
    private StrokeRenderer renderer = new StrokeRenderer();
    private float height = 0;
    private float width = 0;


    public Stroke() {
    }

    @Override
    public Stroke clone() {
        Stroke s = new Stroke();

        for (StrokePoint p : points) {
            s.points.add(p.clone());
        }
        s.computeSize();
        return s;
    }

    public ArrayList<StrokePoint> getPoints() {
        return points;
    }

    public void onPointerMoved(float x, float y) {
        addPoint(new Vector2(x, y));
    }

    public void onPointerMoved(Vector2 position) {
        addPoint(position);
    }

    public void addPoint(Vector2 position) {
        boolean newPoint = false;

        if (!points.isEmpty()) {
            if (points.get(points.size() - 1).getPosition().dst(position) > DISTANCE_THRESHOLD) {
                newPoint = true;
            }
        }

        if (clock.getElapsed() > TIME_THRESHOLD) {
            newPoint = true;
        }

        if (newPoint) {
            StrokePoint p = new StrokePoint(position);
            p.setBehavior(new StrokePointLerpBehavior(StrokePoint.DEFAULT_MAX_RADIUS, StrokePoint.DEFAULT_MIN_RADIUS, StrokePoint.DEFAULT_LIFETIME));
            points.add(p);
            clock.tick();
            computeSize();
        }
    }

    public void setPointRadius(float _pointRadius) {
        for (StrokePoint point : points) {
            point.setRadius(_pointRadius);
        }
    }

    public void draw() {
        if (points.isEmpty() || points.size() < 2) {
            return;
        }
        renderer.draw(this);
    }

    public void act(float delta) {
        clock.act(delta);

        // Remove extra points from the tail
        while (points.size() > MAX_POINTS) {
            points.remove(0);
        }

        // Update points
        for (StrokePoint p : points) {
            p.act(delta);
        }

        int i = points.size() - 1;
        while (i >= 0) {
            if (!points.get(i).isAlive()) {
                points.remove(i);
            }
            --i;
        }
    }

    public boolean isAlive() {
        for (StrokePoint point : points) {
            if (point.isAlive()) {
                return true;
            }
        }
        return false;
    }

    public float getLength() {
        float len = 0;
        Vector2 tmp;
        for (int i = 1; i < points.size(); i++) {
            tmp = points.get(i).getPosition();
            len += tmp.dst(points.get(i - 1).getPosition());
        }
        return len;
    }

    public float getAngleSum() {
        float aSum = 0;
        for (int i = points.size() - 2; i > 0; i--) {
            Vector2 v1 = points.get(i - 1).getPosition();
            Vector2 v2 = points.get(i).getPosition();
            Vector2 v3 = points.get(i + 1).getPosition();
            Vector2 seg1 = v2.cpy().sub(v1);
            Vector2 seg2 = v3.cpy().sub(v2);
            aSum += Math.abs(seg1.angle(seg2));
        }
        return aSum;
    }

    public void setScale(float scale) {
        normalizeInplace();
        for (StrokePoint p : getPoints()) {
            p.setPosition(p.getPosition().scl(scale));
        }
        computeSize();
    }

    public void normalizeInplace() {
        if (points.size() < 2)
            return;
        //Get min max x/y pos
        Vector2 pos = points.get(0).getPosition();
        Vector2 min = new Vector2(pos.x, pos.y);
        Vector2 max = new Vector2(pos.x, pos.y);
        for (int i = 1; i < points.size(); i++) {
            pos = points.get(i).getPosition();
            min.x = Math.min(pos.x, min.x);
            min.y = Math.min(pos.y, min.y);
            max.x = Math.max(pos.x, max.x);
            max.y = Math.max(pos.y, max.y);
        }
        float width = max.x - min.x;
        float height = max.y - min.y;
        float scale = Math.max(width, height);
        scale = 1f / scale;

        for (StrokePoint p : points) {
            p.getPosition().x -= min.x;
            p.getPosition().y -= min.y;
            p.getPosition().scl(scale);
        }
    }

    public void setCenter(Vector2 position) {
        Vector2 meanPosition = new Vector2();
        for (StrokePoint p : getPoints()) {
            meanPosition.add(p.getPosition());
        }
        meanPosition.scl(1f / getPoints().size());

        for (StrokePoint p : getPoints()) {
            p.getPosition().x -= meanPosition.x;
            p.getPosition().y -= meanPosition.y;
            p.getPosition().x += position.x;
            p.getPosition().y += position.y;
        }
    }

    public void setBehavior(StrokePointBehaviorInterface behavior) {
        for (StrokePoint p : getPoints()) {
            p.setBehavior(behavior);
        }
    }

    /**
     * Computes width and heigt
     */
    private void computeSize() {
        Vector2 max = new Vector2(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
        Vector2 min = new Vector2(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
        for (StrokePoint point : points) {
            Vector2 pointPosition = point.getPosition();
            max.x = Math.max(pointPosition.x, max.x);
            max.y = Math.max(pointPosition.y, max.y);
            min.x = Math.min(pointPosition.x, min.x);
            min.y = Math.min(pointPosition.y, min.y);
        }
        width = max.x - min.x;
        height = max.y - min.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
