package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;

/**
 * Stores known strokes
 * Created by hub on 8/17/15.
 */
public class StrokeDatabase {
    ArrayList<KnownStroke> strokes;
    StrokeNormalizer normalizer;

    public StrokeDatabase(String configFile) {
        strokes = new ArrayList<KnownStroke>();
        normalizer = new StrokeNormalizer();
        JsonReader jsonReader = new JsonReader();
        JsonValue rootNode = jsonReader.parse(Gdx.files.internal(configFile));
        for (JsonValue entry = rootNode.child; entry != null; entry = entry.next) {
            strokes.add(fromJson(entry));
        }
    }

    public KnownStroke fromJson(JsonValue node) {
        KnownStroke kStroke = new KnownStroke();
        kStroke.setName(node.getString("name"));
        JsonValue points = node.get("points");
        for (JsonValue p = points.child; p != null; p = p.next) {
            kStroke.addPoint(new Vector2(p.getFloat("x"), p.getFloat("y")));
        }
        normalizer.cleanUp(kStroke);
        return kStroke;
    }

    public ArrayList<KnownStroke> getStrokes() {
        return strokes;
    }
}
