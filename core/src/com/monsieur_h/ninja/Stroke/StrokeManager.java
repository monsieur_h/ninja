package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.Gdx;
import com.monsieur_h.ninja.Stroke.comparators.StrokeComparatorCollection;

/**
 * A container object used to normalizeInplace, cleanup, and match Strokes against a database
 * Created by hubert on 25/08/15.
 */
public class StrokeManager {
    private static final float MATCH_THRESHOLD = .85f;
    private StrokeNormalizer normalizer;
    private StrokeDatabase database;
    private StrokeComparatorCollection comparatorCollection;

    public StrokeManager(String databaseFilename) {
        database = new StrokeDatabase(databaseFilename);
        normalizer = new StrokeNormalizer();
        comparatorCollection = new StrokeComparatorCollection();
    }

    /**
     * Compares a stroke to the database of KnownStrokes and returns the best result
     *
     * @param stroke Stroke to compare
     * @return Best result
     */
    public KnownStroke recognize(Stroke stroke) {
        Stroke test = stroke.clone();
        normalizer.cleanUp(test);

        KnownStroke maxMatch = null;
        float maxScore = 0;
        for (KnownStroke kn : database.getStrokes()) {
            float matchScore = comparatorCollection.matchScore(test, kn);
            if (matchScore > maxScore
                    && matchScore > MATCH_THRESHOLD) {
                maxScore = matchScore;
                maxMatch = kn;
            }
            Gdx.app.log("RECOGNIZE", "Found a score of " + matchScore + " for " + kn.getName());
        }
        return maxMatch;
    }

    public StrokeComparatorCollection getComparatorCollection() {
        return comparatorCollection;
    }

    public StrokeNormalizer getNormalizer() {
        return normalizer;
    }
}
