package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Prepares a Stroke for analysis.
 * Handles normalization and removal of unused points
 * Created by hubert on 20/08/15.
 */
public class StrokeNormalizer {
    private static final float DEFAULT_ANGLE_THRESHOLD = 15f;
    private static final float DEFAULT_PROXIMITY_THRESHOLD = .015f;

    public StrokeNormalizer() {
    }

    public void removeByAngle(Stroke stroke) {
        removeByAngle(stroke, DEFAULT_ANGLE_THRESHOLD);
    }

    public void removeByAngle(Stroke stroke, float angle_threshold) {
        ArrayList<StrokePoint> points = stroke.getPoints();
        for (int i = points.size() - 2; i > 0; i--) {
            Vector2 v1 = points.get(i - 1).getPosition();
            Vector2 v2 = points.get(i).getPosition();
            Vector2 v3 = points.get(i + 1).getPosition();
            Vector2 seg1 = v2.cpy().sub(v1);
            Vector2 seg2 = v3.cpy().sub(v2);
            if (Math.abs(seg1.angle(seg2)) < angle_threshold) {
                points.remove(i);
            }
        }
    }

    public void removeByProximity(Stroke stroke) {
        removeByProximity(stroke, DEFAULT_PROXIMITY_THRESHOLD);
    }

    public void removeByProximity(Stroke stroke, float proximity) {

    }

    public void setMaxPoint(Stroke stroke, int maxPoints) {

    }

    /**
     * Normalizes a stroke and clean up points using default thresholds
     *
     * @param stroke
     */
    public void cleanUp(Stroke stroke) {
        stroke.normalizeInplace();
        removeByAngle(stroke);
        removeByProximity(stroke);
    }
}
