package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Stroke.behavior.StrokePointBehaviorInterface;
import com.monsieur_h.ninja.Stroke.behavior.StrokePointIdleBehavior;

/**
 * A point composing a line
 * Created by hub on 07/08/15.
 */
public class StrokePoint {
    public static final float DEFAULT_MAX_RADIUS = 30f;
    public static final float DEFAULT_MIN_RADIUS = 15f;
    public static final float DEFAULT_LIFETIME = .5f;
    StrokePointBehaviorInterface behavior;
    private Vector2 position;
    private float radius = DEFAULT_MAX_RADIUS;
    private Color color = new Color(Color.WHITE);
    private boolean alive = true;

    public StrokePoint(Vector2 initialPosition) {
        init(initialPosition);
        behavior = new StrokePointIdleBehavior();
    }

    public StrokePoint(float x, float y) {
        init(new Vector2(x, y));
    }

    @Override
    public StrokePoint clone() {
        StrokePoint p = new StrokePoint(getPosition());
        p.radius = radius;
        return p;
    }

    public void setPosition(Vector2 initialPosition) {
        init(initialPosition);
    }

    public void setPosition(float x, float y) {
        init(new Vector2(x, y));
    }

    private void init(Vector2 initialPosition) {
        position = initialPosition.cpy();

    }

    public void act(float delta) {
        if (behavior != null) {
            behavior.act(delta, this);
        }
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setBehavior(StrokePointBehaviorInterface behavior) {
        this.behavior = behavior;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
}
