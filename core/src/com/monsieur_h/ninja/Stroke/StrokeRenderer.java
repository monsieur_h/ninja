package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Used to draw a Stroke
 * Created by hubert on 31/08/15.
 */
public class StrokeRenderer {
    private ShapeRenderer renderer = new ShapeRenderer();
    private Color color = new Color(Color.WHITE);

    public StrokeRenderer() {
    }

    protected void startDraw() {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(color);
    }

    protected void endDraw() {
        renderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void draw(Stroke stroke) {
        if (stroke.getPoints().size() < 2) {
            return;
        }

        //Compute left/right point for each StrokePoint
        ArrayList<Anchor> anchors = new ArrayList<Anchor>(stroke.getPoints().size());
        anchors.add(0, new Anchor(stroke.getPoints().get(0), stroke.getPoints().get(0), stroke.getPoints().get(1)));
        for (int i = 1; i <= stroke.getPoints().size() - 2; i++) {
            anchors.add(new Anchor(
                    stroke.getPoints().get(i),
                    stroke.getPoints().get(i - 1),
                    stroke.getPoints().get(i + 1)));
        }
        int size = stroke.getPoints().size();
        anchors.add(new Anchor(stroke.getPoints().get(size - 1), stroke.getPoints().get(size - 2), stroke.getPoints().get(size - 1)));
        anchors.add(new Anchor(stroke.getPoints().get(size - 1), stroke.getPoints().get(size - 2), stroke.getPoints().get(size - 1)));
        //TODO: Move above code to update method


        startDraw();

        //Actual draw
        for (int i = 0; i < anchors.size() - 1; i++) {
            drawAnchor(anchors.get(i));
        }


//        //Draw first circle
        drawStrokeEnd(stroke.getPoints().get(0), stroke.getPoints().get(1));
//        //Draw last point
        drawStrokeEnd(stroke.getPoints().get(stroke.getPoints().size() - 1), stroke.getPoints().get(stroke.getPoints().size() - 2));
        endDraw();
    }

    /**
     * Draws a half-disc at the end of the stroke
     *
     * @param outerEnd
     * @param previous
     */
    private void drawStrokeEnd(StrokePoint outerEnd, StrokePoint previous) {
        Vector2 direction = previous.getPosition().cpy().sub(outerEnd.getPosition()).rotate90(1);
        Color previousColor = renderer.getColor();
        renderer.setColor(outerEnd.getColor());
        renderer.arc(outerEnd.getPosition().x, outerEnd.getPosition().y, outerEnd.getRadius(), direction.angle(), 180f);
        renderer.setColor(previousColor);
    }

    /**
     * Draws a segment between two points
     *
     * @param current
     */
    private void drawAnchor(Anchor current) {
        Color blue = new Color(0, 0, 1, .2f);
        Color red = new Color(1, 0, 0, .2f);
        //Render previous <--> current
        for (int i = 0; i < current.triangles.length; i++) {
            renderer.triangle(
                    current.triangles[i].a.x,
                    current.triangles[i].a.y,

                    current.triangles[i].b.x,
                    current.triangles[i].b.y,

                    current.triangles[i].c.x,
                    current.triangles[i].c.y,

                    current.triangles[i].a.color,
                    current.triangles[i].b.color,
                    current.triangles[i].c.color
            );
        }

        if (current.jointTriangle != null) {
            renderer.setColor(current.point.getColor());
            renderer.arc(current.jointTriangle.c.x, current.jointTriangle.c.y,
                    current.arcLen,
                    current.arcAngleStart,
                    current.arcAngleEnd, 12
            );
        }
    }

    /**
     * Holds a StrokePoint and a left/right point according to its radius
     */
    private class Anchor {
        public VertexTriangle[] triangles = new VertexTriangle[4];
        public VertexTriangle jointTriangle;
        float arcAngleEnd;
        private StrokePoint point;
        private Vector2 nRight;
        private Vector2 nLeft;
        private Vector2 pRight;
        private Vector2 pLeft;
        private Vector2 cpLeft;
        private Vector2 cpRight;
        private Vector2 cnRight;
        private Vector2 cnLeft;
        private Vector2 jointBase;
        private float arcAngleStart;
        private float arcLen;

        public Anchor(StrokePoint point, StrokePoint previous, StrokePoint next) {
            this.point = point;
            Vector2 previousPos = previous.getPosition().cpy().add(point.getPosition()).scl(1f / 2f);
            Vector2 nextPos = next.getPosition().cpy().add(point.getPosition()).scl(1f / 2f);//TODO: Move the split somewhere else (before call to Ctor ?)
            computePoints(point, previousPos, nextPos, previous.getRadius(), next.getRadius());
            computeJointPos(point, previous, next);
            computeTriangles(point, previous, next);
        }

        private void computePoints(StrokePoint point, Vector2 previous, Vector2 next, float previousRadius, float nextRadius) {
            previousRadius = (previousRadius + point.getRadius()) / 2;
            nextRadius = (nextRadius + point.getRadius()) / 2;

            Vector2 perpendicularPreviousSegment = point.getPosition().cpy().sub(previous).nor().rotate90(1);
            Vector2 perpendicularNextSegment = point.getPosition().cpy().sub(next).nor().rotate90(1);

            pLeft = previous.cpy().add(perpendicularPreviousSegment.cpy().scl(previousRadius));
            pRight = previous.cpy().sub(perpendicularPreviousSegment.cpy().scl(previousRadius));

            nLeft = next.cpy().add(perpendicularNextSegment.cpy().scl(nextRadius));
            nRight = next.cpy().sub(perpendicularNextSegment.cpy().scl(nextRadius));

            cpLeft = point.getPosition().cpy().add(perpendicularPreviousSegment.cpy().scl(point.getRadius()));
            cpRight = point.getPosition().cpy().sub(perpendicularPreviousSegment.cpy().scl(point.getRadius()));

            cnLeft = point.getPosition().cpy().add(perpendicularNextSegment.cpy().scl(point.getRadius()));
            cnRight = point.getPosition().cpy().sub(perpendicularNextSegment.cpy().scl(point.getRadius()));

            arcAngleEnd = perpendicularNextSegment.angle(perpendicularPreviousSegment);
        }

        private void computeJointPos(StrokePoint current, StrokePoint previous, StrokePoint next) {
            // calculate which point should be merged based on the way the anchor bends
            jointTriangle = new VertexTriangle();
            if (arcAngleEnd > 0) {
                jointBase = intersection(
                        nRight.x, nRight.y,
                        cnRight.x, cnRight.y,

                        pLeft.x, pLeft.y,
                        cpLeft.x, cpLeft.y
                );

                jointTriangle.a = new Vertex(cpRight.cpy(), current.getColor());
                jointTriangle.b = new Vertex(cnLeft.cpy(), current.getColor());
                cnRight = (jointBase == null) ? cnRight : jointBase;
                cpLeft = (jointBase == null) ? cpLeft : jointBase;
            } else {
                jointBase = intersection(
                        nLeft.x, nLeft.y,
                        cnLeft.x, cnLeft.y,

                        pRight.x, pRight.y,
                        cpRight.x, cpRight.y
                );

                jointTriangle.a = new Vertex(cpLeft.cpy(), current.getColor());
                jointTriangle.b = new Vertex(cnRight.cpy(), current.getColor());
                cnLeft = (jointBase == null) ? cnLeft : jointBase;
                cpRight = (jointBase == null) ? cpRight : jointBase;
            }

            if (jointBase == null) {
                jointTriangle = null;
            } else {
                jointTriangle.c = new Vertex(jointBase, current.getColor());
                Vector2 ab = new Vector2(jointTriangle.b.x, jointTriangle.b.y).sub(jointTriangle.a.x, jointTriangle.a.y);
                if (ab.crs(jointBase.x, jointBase.y) < 0) {
                    jointTriangle = null;
                    return;
                }

                Vector2 ac = new Vector2(jointTriangle.a.x, jointTriangle.a.y).sub(jointBase);
                Vector2 bc = new Vector2(jointTriangle.b.x, jointTriangle.b.y).sub(jointBase);

                arcAngleStart = ac.angle();
                arcAngleEnd = ac.angle(bc);
                arcLen = new Vector2(jointTriangle.a.x, jointTriangle.a.y).dst(jointBase);
            }
        }

        public Vector2 intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
            float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
            if (d == 0) return null;

            float xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
            float yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;

            return new Vector2(xi, yi);
        }

        private void computeTriangles(StrokePoint current, StrokePoint previous, StrokePoint next) {
            Color previousColor = new Color(previous.getColor());
            previousColor.r = (previousColor.r + current.getColor().r) / 2;
            previousColor.g = (previousColor.g + current.getColor().g) / 2;
            previousColor.b = (previousColor.b + current.getColor().b) / 2;
            previousColor.a = (previousColor.a + current.getColor().a) / 2;
            Color nextColor = new Color(next.getColor());
            nextColor.r = (nextColor.r + current.getColor().r) / 2;
            nextColor.g = (nextColor.g + current.getColor().g) / 2;
            nextColor.b = (nextColor.b + current.getColor().b) / 2;
            nextColor.a = (nextColor.a + current.getColor().a) / 2;

            triangles[0] = new VertexTriangle(
                    new Vertex(pRight, previousColor),
                    new Vertex(pLeft, previousColor),
                    new Vertex(cpRight, current.getColor())
            );
            triangles[1] = new VertexTriangle(
                    new Vertex(cpRight, current.getColor()),
                    new Vertex(pLeft, previousColor),
                    new Vertex(cpLeft, current.getColor())
            );
            triangles[2] = new VertexTriangle(
                    new Vertex(nRight, nextColor),
                    new Vertex(nLeft, nextColor),
                    new Vertex(cnRight, current.getColor())
            );
            triangles[3] = new VertexTriangle(
                    new Vertex(cnRight, current.getColor()),
                    new Vertex(nLeft, nextColor),
                    new Vertex(cnLeft, current.getColor())
            );
        }
    }
}
