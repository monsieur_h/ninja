package com.monsieur_h.ninja.Stroke;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Just that. A Vertex
 * Created by hubert on 03/09/15.
 */
public class Vertex {
    public Color color = Color.WHITE;
    public float x, y;

    public Vertex(Vector2 position, Color color) {
        this.x = position.x;
        this.y = position.y;
        this.color = color;
    }

    public Vertex(Vector2 position) {
        this.x = position.x;
        this.y = position.y;
    }
}
