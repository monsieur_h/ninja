package com.monsieur_h.ninja.Stroke;

/**
 * Created by hubert on 03/09/15.
 */
public class VertexTriangle {
    public Vertex a, b, c;

    public VertexTriangle(Vertex a, Vertex b, Vertex c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public VertexTriangle() {
    }
}
