package com.monsieur_h.ninja.Stroke.behavior;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Created by hubert on 31/08/15.
 */
public class ColorAndSizeBehavior implements StrokePointBehaviorInterface {
    private final float duration;
    private StrokePointColorBehavior colorBehavior;
    private StrokePointLerpBehavior lerpBehavior;
    private float elapsed = 0;

    public ColorAndSizeBehavior(float initialSize, float finalSize, Color startColor, Color endColor, float duration) {
        lerpBehavior = new StrokePointLerpBehavior(initialSize, finalSize, duration);
        colorBehavior = new StrokePointColorBehavior(startColor, endColor, duration);
        this.duration = duration;
    }

    @Override
    public void act(float delta, StrokePoint p) {
        elapsed += delta;
        lerpBehavior.act(delta, p);
        colorBehavior.act(delta, p);
        if (elapsed > duration) {
            p.setAlive(false);
        }
    }

    public void setElapsed(float elapsed) {
        lerpBehavior.setElapsed(elapsed);
        colorBehavior.setElapsed(elapsed);
        this.elapsed = elapsed;
    }

}
