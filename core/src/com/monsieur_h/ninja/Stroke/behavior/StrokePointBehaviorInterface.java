package com.monsieur_h.ninja.Stroke.behavior;

import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Created by hubert on 19/08/15.
 */
public interface StrokePointBehaviorInterface {
    void act(float delta, StrokePoint p);
}
