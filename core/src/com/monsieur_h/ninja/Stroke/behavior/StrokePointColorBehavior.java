package com.monsieur_h.ninja.Stroke.behavior;

import com.badlogic.gdx.graphics.Color;
import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Created by hubert on 31/08/15.
 */
public class StrokePointColorBehavior implements StrokePointBehaviorInterface {
    private Color start;
    private Color end;
    private Color current = new Color();
    private float duration;
    private float elapsed = 0;

    public StrokePointColorBehavior(Color start, Color end, float duration) {
        this.start = start;
        this.end = end;
        this.duration = duration;
        current = start.cpy();
    }

    @Override
    public void act(float delta, StrokePoint p) {
        elapsed += delta;

        float ratio = elapsed / duration;
        ratio = Math.min(ratio, 1f);
        current.r = start.r + (end.r - start.r) * ratio;
        current.g = start.g + (end.g - start.g) * ratio;
        current.b = start.b + (end.b - start.b) * ratio;
        current.a = start.a + (end.a - start.a) * ratio;
        p.setColor(current.cpy());
    }

    public void setElapsed(float elapsed) {
        this.elapsed = elapsed;
    }
}
