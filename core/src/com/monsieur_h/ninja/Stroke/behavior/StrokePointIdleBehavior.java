package com.monsieur_h.ninja.Stroke.behavior;

import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Idle behavior...
 * Created by hubert on 19/08/15.
 */
public class StrokePointIdleBehavior implements StrokePointBehaviorInterface {
    @Override
    public void act(float delta, StrokePoint p) {
        return;
    }
}
