package com.monsieur_h.ninja.Stroke.behavior;

import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Decreases the size of the point
 * Created by hubert on 19/08/15.
 */
public class StrokePointLerpBehavior implements StrokePointBehaviorInterface {
    private float duration;
    private float elapsed;
    private float initialSize;
    private float finalSize;

    public StrokePointLerpBehavior(float initialSize, float finalSize, float duration) {
        //  40 - 5 - 3
        this.initialSize = initialSize;
        this.finalSize = finalSize;
        this.duration = duration;
        elapsed = 0;
    }

    @Override
    public void act(float delta, StrokePoint p) {
        elapsed += delta;
        if (elapsed > duration) {
            p.setBehavior(new StrokePointIdleBehavior());
            return;
        }
        float ratio = elapsed / duration;
        p.setRadius(initialSize + ratio * (finalSize - initialSize));
    }

    public void setElapsed(float elapsed) {
        this.elapsed = elapsed;
    }
}
