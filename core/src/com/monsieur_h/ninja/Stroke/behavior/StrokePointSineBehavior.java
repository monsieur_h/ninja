package com.monsieur_h.ninja.Stroke.behavior;

import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Endless sine behavior
 * Created by hubert on 19/08/15.
 */
public class StrokePointSineBehavior implements StrokePointBehaviorInterface {
    private final float minSize;
    private final float maxSize;
    private float elapsed;
    private float frequency;

    public StrokePointSineBehavior(float minSize, float maxSize, float frequency) {
        this.frequency = frequency;
        this.minSize = minSize;
        this.maxSize = maxSize;
        elapsed = (float) (Math.random() * Math.PI);
    }

    @Override
    public void act(float delta, StrokePoint p) {
        elapsed += delta;
        float ratio = (float) Math.sin(elapsed * frequency);
        ratio += 1;
        ratio /= 2;
        p.setRadius((ratio * (maxSize - minSize)) + minSize);
    }
}
