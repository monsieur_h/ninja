package com.monsieur_h.ninja.Stroke.comparators;

import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Compares Strokes
 * Created by hubert on 17/08/15.
 */
public class AngleComparator implements StrokeComparatorInterface {
    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        float minAngle = Math.min(s1.getAngleSum(), s2.getAngleSum());
        float maxAngle = Math.max(s1.getAngleSum(), s2.getAngleSum());
        return (maxAngle >= 0.01f) ? minAngle / maxAngle : 1;
    }
}
