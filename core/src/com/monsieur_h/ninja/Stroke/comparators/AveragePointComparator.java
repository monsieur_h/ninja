package com.monsieur_h.ninja.Stroke.comparators;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Stroke.Stroke;
import com.monsieur_h.ninja.Stroke.StrokePoint;

/**
 * Compares the center of both Strokes
 * Created by hubert on 18/08/15.
 */
public class AveragePointComparator implements StrokeComparatorInterface {
    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        Vector2 meanS1 = new Vector2();
        for (StrokePoint p : s1.getPoints()) {
            meanS1.add(p.getPosition());
        }
        meanS1.scl(1f / (float) s1.getPoints().size());

        Vector2 meanS2 = new Vector2();
        for (StrokePoint p : s2.getPoints()) {
            meanS2.add(p.getPosition());
        }
        meanS2.scl(1f / (float) s2.getPoints().size());

        float maxDistance = new Vector2(1f, 1f).len();
        float pointDistance = meanS1.dst(meanS2);
        return (maxDistance - pointDistance) / maxDistance;
    }
}
