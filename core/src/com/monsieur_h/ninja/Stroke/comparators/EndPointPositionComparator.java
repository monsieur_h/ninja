package com.monsieur_h.ninja.Stroke.comparators;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Compares positions of the last point of each shape
 * Created by hubert on 07/09/15.
 */
public class EndPointPositionComparator implements StrokeComparatorInterface {
    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        Vector2 p1 = s1.getPoints().get(s1.getPoints().size() - 1).getPosition();
        Vector2 p2 = s2.getPoints().get(s2.getPoints().size() - 1).getPosition();

        float maxDistance = new Vector2(1f, 1f).len();
        float pointDistance = p1.dst(p2);
        return (maxDistance - pointDistance) / maxDistance;
    }
}
