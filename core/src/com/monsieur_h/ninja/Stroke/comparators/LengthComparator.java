package com.monsieur_h.ninja.Stroke.comparators;

import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Compares Strokes based on their length
 * Created by hubert on 17/08/15.
 */
public class LengthComparator implements StrokeComparatorInterface {
    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        float minScore = Math.min(s1.getLength(), s2.getLength());
        float maxScore = Math.max(s1.getLength(), s2.getLength());
        return minScore / maxScore;
    }
}
