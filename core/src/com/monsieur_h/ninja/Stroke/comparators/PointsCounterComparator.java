package com.monsieur_h.ninja.Stroke.comparators;

import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Compares the number of points
 * Created by hub on 8/17/15.
 */
public class PointsCounterComparator implements StrokeComparatorInterface {
    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        float maxPoints = Math.max(s1.getPoints().size(), s2.getPoints().size());
        float minPoints = Math.min(s1.getPoints().size(), s2.getPoints().size());
        return minPoints / maxPoints;
    }
}
