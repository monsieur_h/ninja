package com.monsieur_h.ninja.Stroke.comparators;

import com.monsieur_h.ninja.Stroke.Stroke;

import java.util.ArrayList;

/**
 * A collection object holding a.. hum.. collection of comparator to return a score
 * Created by hubert on 17/08/15.
 */
public class StrokeComparatorCollection implements StrokeComparatorInterface {
    private ArrayList<WeightContainer> comparators;

    public StrokeComparatorCollection() {
        comparators = new ArrayList<WeightContainer>();
    }

    public void add(StrokeComparatorInterface s) {
        comparators.add(new WeightContainer(s));
    }

    public void add(StrokeComparatorInterface s, float weight) {
        assert (weight > 0);
        comparators.add(new WeightContainer(s, weight));
    }

    @Override
    public boolean match(Stroke s1, Stroke s2, float threshold) {
        return matchScore(s1, s2) > threshold;
    }

    @Override
    public float matchScore(Stroke s1, Stroke s2) {
        float mean = 0;
        float amount = 0;
        if (s1.getPoints().size() < 2 || s2.getPoints().size() < 2)
            return 0;
        for (WeightContainer container : comparators) {
            mean += container.comparator.matchScore(s1, s2) * container.weight;
            amount += container.weight;
        }
        return mean / amount;
    }

    private class WeightContainer {
        public float weight;
        public StrokeComparatorInterface comparator;

        public WeightContainer(StrokeComparatorInterface comp, float weight) {
            init(comp, weight);
        }

        public WeightContainer(StrokeComparatorInterface comp) {
            init(comp, 1f);
        }

        private void init(StrokeComparatorInterface comp, float weight) {
            this.weight = weight;
            this.comparator = comp;
        }
    }
}
