package com.monsieur_h.ninja.Stroke.comparators;

import com.monsieur_h.ninja.Stroke.Stroke;

/**
 * Created by hubert on 17/08/15.
 */
public interface StrokeComparatorInterface {
    boolean match(Stroke s1, Stroke s2, float threshold);

    float matchScore(Stroke s1, Stroke s2);
}
