package com.monsieur_h.ninja.Util;

/**
 * A simple clock that ticks
 * Created by hub on 07/08/15.
 */
public class Clock {
    private double lastTick;
    private double time;

    public Clock() {
        lastTick = 0;
        time = 0;
    }

    /**
     * Returns the time since the last call to tick
     *
     * @return time since last tick
     */
    public float tick() {
        float elapsed = getElapsed();
        lastTick = time;
        return elapsed;
    }

    /**
     * Returns the elapsed time since last tick
     *
     * @return time since last tick
     */
    public float getElapsed() {
        return (float) (time - lastTick);
    }

    /**
     * Updates the time
     *
     * @param delta elapsed time in ms
     */
    public void act(float delta) {
        time += delta;
    }

}
