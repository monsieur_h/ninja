package com.monsieur_h.ninja.Util;

/**
 * Base class for events
 * Created by hub on 8/16/15.
 */
public abstract class Event {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
