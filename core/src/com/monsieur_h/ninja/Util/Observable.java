package com.monsieur_h.ninja.Util;

import java.util.ArrayList;

/**
 * A simple observable class for the observer pattern
 * Created by hub on 8/16/15.
 */
public class Observable {
    private ArrayList<Observer> observers;

    public Observable() {
        observers = new ArrayList<Observer>();
    }

    public void register(Observer o) {
        observers.add(o);
    }

    public void unregister(Observer o) {
        observers.remove(o);
    }

    public void unregisterAll() {
        while (!observers.isEmpty())
            observers.remove(0);
    }

    public void dispatch(Event e) {
        for (Observer o : observers) {
            o.onEvent(e);
        }
    }

}
