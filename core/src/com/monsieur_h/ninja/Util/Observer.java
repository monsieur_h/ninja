package com.monsieur_h.ninja.Util;

/**
 * Interface for observer pattern
 * Created by hub on 8/16/15.
 */
public interface Observer {
    void onEvent(Event e);
}
